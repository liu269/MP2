<h1>Configure & Run</h1>
<h2>Membership Service</h2>
Open <code>MP2/replicate.sh</code> and put IP addresses of all VM's in <code>IPs</code> array variable, then change the IP <code>172.22.149.118</code> to the actual IP of VM that clones MP2 from git. Similary, put all those IP addressed in <code>MP2/service/logger/IP.txt</code>, one IP per line. 

<h2>Set Up Passwordless SSH</h2>
On the VM that clones MP2 from git, enable this VM's ssh into other VM's without password. This step is optional, though, if you are OK with typing ssh password in the next step.

```bash
ssh-keygen
ssh-copy-id -i ~/.ssh/id_rsa.pub remote_host_ip
```
<code>remote_host_ip</code> is the IP addresses of the other VM's

<h2>Run MP2</h2>
Run the script <code>replicate.sh</code> to distribute MP2 source code into all other VM's, the run <code>MP2/../service/start-all.sh</code> to start the service. At each VM, wait before prompt gets returned, then proceed to the next VM to repeat the process. If you get error of <code>...address already in use...</code>, you can reuse the address after running <code>MP2/../service/reset.sh</code> and running <code>MP2/../service/start-all.sh</code> again

To view the membership list of all VM's, <code>cd</code> into <code>MP2/../service/logger</code> and run <code>querier.sh</code>. When a VM crashes or is stopped(<code>MP2/../service/stop-all.sh</code>), all alive VM's will update their membership lists and <code>querier.sh</code> will show you these changes. 
