#!/bin/bash

### distribute photon source code to all other nodes ###
rm -rf ../service < /dev/null
cp -r service ../

IPs=(172.22.149.119 172.22.149.120 172.22.149.121 172.22.149.122 172.22.149.123 172.22.149.124 172.22.149.125 172.22.149.126 172.22.149.127)

for IP in "${IPs[@]}"
do
	ssh "${IP}" "rm -rf service" < /dev/null
	scp -r ./service "${IP}":
	ssh "${IP}" 'printf "${IP}" > ./service/logger/selfIP.txt'
done

printf "172.22.149.118" > ~/service/logger/selfIP.txt
