#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <string.h>

#define TRANSFER_BUFFER 60000

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *socket_address)
{
	if(socket_address->sa_family == AF_INET)
	{
		return &( ((struct sockaddr_in*)socket_address)->sin_addr );
	}

	return &( ((struct sockaddr_in6*)socket_address)->sin6_addr );
}

int main(int argument_number, char *argument_vector[])
{
	if(argument_number != 2)
	{
	    fprintf(stderr,"usage: address of the server+slash+file name\n");
	    exit(1);
	}

	// extract hostname, port number, file path
	char host_name[ strlen(argument_vector[1])+1 ];
	char port_number[ strlen(argument_vector[1])+1 ];
	char path[ strlen(argument_vector[1])+1 ];

	memset(&host_name, 0, sizeof host_name); 
	memset(&port_number, 0, sizeof port_number); 
	memset(&path, 0, sizeof path); 

	if( strchr(argument_vector[1], ':') == NULL ) // port number is not provided
	{
		char *slash = strchr(argument_vector[1], '/');
		*slash = '\0';

		strcpy(host_name, argument_vector[1]);

		strcpy(port_number, "80");

		slash = sizeof(char)+slash;
		strcpy(path, slash);
	}
	else
	{
		char *colon = strchr(argument_vector[1], ':');
		char *slash = strchr(argument_vector[1], '/');
		*colon = '\0';
		*slash = '\0';

		strcpy(host_name, argument_vector[1]);

		colon = sizeof(char)+colon;
		strcpy(port_number, colon);

		slash = sizeof(char)+slash;
		strcpy(path, slash);
		
	}

	// define hints;
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints); 
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	int result;
	struct addrinfo *server_information;
	if( (result = getaddrinfo(host_name, port_number, &hints, &server_information)) != 0 )
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(result));
		return 1;
	}

	// loop through all the results and connect to the first we can
	struct addrinfo *temp;
	int socket_descriptor;
	for(temp = server_information; temp != NULL; temp = temp->ai_next)
	{
		// get socket descriptor
		if( (socket_descriptor = socket(temp->ai_family, temp->ai_socktype, temp->ai_protocol)) == -1 )
		{
			perror("client: socket");
			continue;
		}

		// connect socket to server
		if(connect(socket_descriptor, temp->ai_addr, temp->ai_addrlen) == -1)
		{
			close(socket_descriptor);
			//perror("client: connect");
			continue;
		}

		break;
	}

	if(temp == NULL)
	{
		//fprintf(stderr, "client: failed to connect\n");
		return 2;
	}

	// convert network IP representation to readable IP
	char ip_read[INET6_ADDRSTRLEN];
	memset(&ip_read, 0, sizeof ip_read); 
	inet_ntop(temp->ai_family, get_in_addr( (struct sockaddr *)temp->ai_addr ), ip_read, sizeof ip_read);
	//printf("client: connecting to %s\n", ip_read);

	freeaddrinfo(server_information); // all done with this structure

	// send out request message
	// find HTTP version:
	char http_version[10];
	memset(&http_version, 0, sizeof http_version); 
	strcpy(http_version, " HTTP/1.0");

	// construct message and send message via send()
	char message[ 5+strlen(path)+strlen(http_version)+4+1 ];
	memset(&message, 0, sizeof message);
	strcpy(message, "GET /");
	strcat(message, path);
	strcat(message, http_version);
	strcat(message, "\r\n\r\n");

	if(
	   send(socket_descriptor, message, sizeof(char)*strlen(message), 0) == -1
	  )
	{
		perror("send");
	}

	// read data from server and put it into "output"
	FILE * file;

	// open file
	file = fopen("output", "wb");
	if(file == NULL)
	{
		fputs("File error", stderr);
		exit(1);
	}

	// read input into output file
	char buffer[TRANSFER_BUFFER];
	memset(&buffer, 0, sizeof buffer); 

	int flag = 0; // if flag is 0, end of head has not been encountered
	int recieved;
	while( (recieved = recv(socket_descriptor, buffer, TRANSFER_BUFFER-1, 0)) > 0 )
	{
		buffer[recieved] = '\0';
		if(flag == 0)
		{
			if(strstr(buffer, "\r\n\r\n") != NULL)
			{
				char *start = strstr(buffer, "\r\n\r\n");
				start = start + sizeof(char)*4;
				fputs(start, file);
				flag = 1;
			}
		}
		else
		{
			fputs(buffer, file);
		}
	}

	fclose(file);

	close(socket_descriptor);

	return 0;
}
