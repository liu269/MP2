#!/bin/bash

# This script kills all old processes that prevent all services to start again

sudo kill $(ps -eaf | grep "http" | awk '{print $2}')
sudo kill $(ps -eaf | grep "membership" | awk '{print $2}')
