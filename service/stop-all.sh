#!/bin/bash

### This script stops all services on host ###

# stop logger
cd ./logger
rm http_client
rm http_server
kill $(ps | grep "http_server" | awk '{print $1}')
rm member.txt
rm file_list.txt
cd ../
printf "OK - Logger service stopped\n"

# stop membership service
cd ./membership
kill $(ps | grep "membership_clie" | awk '{print $1}')
kill $(ps | grep "membership_serv" | awk '{print $1}')
rm membership_client
rm membership_server
cd ../
printf "OK - membership serivce stopped\n"
